include device/rockchip/rk3399/BoardConfig.mk

# Sensors
BOARD_SENSOR_ST := false
BOARD_SENSOR_MPU := false
BOARD_SENSOR_MPU_VR := false

# Disable AFBC for HDMI binded to vopb
BOARD_USE_AFBC_LAYER := false

# Enable dex-preoptimization to speed up first boot sequence
ifeq ($(HOST_OS),linux)
  ifneq ($(TARGET_BUILD_VARIANT),eng)
    ifeq ($(WITH_DEXPREOPT),)
      WITH_DEXPREOPT := true
    endif
  endif
endif

TARGET_BOARD_PLATFORM_PRODUCT := tablet

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2147483648

